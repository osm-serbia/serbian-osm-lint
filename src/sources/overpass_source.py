# -*- coding: utf-8 -*-

import http
import socket
import time
import urllib

import overpy
from overpy.exception import OverpassTooManyRequests, OverpassGatewayTimeout

import tools
from sources.osm_source import OSMSource

logger = tools.get_logger(__name__)


def retry_on_error(timeout_in_seconds=5*60):
    def decorate(func):
        def call(*args, **kwargs):
            retries = 5
            while retries > 0:
                try:
                    result = func(*args, **kwargs)
                except (ConnectionRefusedError, OverpassTooManyRequests, OverpassGatewayTimeout,
                        socket.timeout, urllib.error.URLError, http.client.RemoteDisconnected):
                    retries = retries - 1
                    logger.warning('Connection refused, retrying')
                    time.sleep(timeout_in_seconds)
                    continue
                return result
            raise Exception('Exhausted retries for connection refused, quitting')
        return call
    return decorate


class OverpassSource(OSMSource):
    def __init__(self, context, process_entity_callback, map_name, query):
        super(OverpassSource, self).__init__(context, map_name, process_entity_callback, False)
        self.map_name = map_name
        self.query = query
        self.api = overpy.Overpass(url='https://lz4.overpass-api.de/api/interpreter')

    @retry_on_error()
    def _process_map(self):
        results = self.api.query("""
{0}
// &contact=https://gitlab.com/osm-serbia/serbian-osm-lint
        """.format(self.query))

        logger.info('Found {0} nodes, {1} ways, {2} relations for source: {3}'.format(
            len(results.nodes), len(results.ways), len(results.relations), self.map_name))
        for result in results.nodes:
            self._entity_found(result)
        for result in results.ways:
            self._entity_found(result)
        for result in results.relations:
            self._entity_found(result)
