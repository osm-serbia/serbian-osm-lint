# -*- coding: utf-8 -*-

cyr_to_lat = {
    'А': 'A', 'Б': 'B', 'В': 'V', 'Г': 'G', 'Д': 'D', 'Е': 'E',
    'Ж': 'Ž', 'З': 'Z', 'И': 'I', 'Ј': 'J', 'К': 'K', 'Л': 'L',
    'М': 'M', 'Н': 'N', 'Њ': 'Nj', 'О': 'O', 'П': 'P', 'Р': 'R',
    'С': 'S', 'Т': 'T', 'Ћ': 'Ć', 'У': 'U', 'Ф': 'F', 'Х': 'H',
    'Ц': 'C', 'Ч': 'Č', 'Џ': 'Dž', 'Ш': 'Š', 'Ђ': 'Đ', 'Љ': 'Lj',
    'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e',
    'ж': 'ž', 'з': 'z', 'и': 'i', 'ј': 'j', 'к': 'k', 'л': 'l',
    'љ': 'lj','м': 'm', 'н': 'n', 'њ': 'nj', 'о': 'o', 'п': 'p',
    'р': 'r', 'с': 's', 'т': 't', 'ћ': 'ć', 'у': 'u', 'ф': 'f',
    'х': 'h', 'ц': 'c', 'ч': 'č', 'џ': 'dž', 'ш': 'š', 'ђ': 'đ'}

lat_to_int = {
    'Š': 'S', 'š': 's', 'Đ': 'Dj', 'đ': 'dj', 'Č': 'C', 'č': 'c', 'Ć': 'C', 'ć': 'c', 'Ž': 'Z', 'ž': 'z'
}

def at_least_some_in_cyrillic(s):
    return any(ch for ch in s if ch in cyr_to_lat)


def at_least_some_in_latin(s):
    return any(ch for ch in s if ch in cyr_to_lat.values())


def cyr2lat(text):
    out = ''
    for c in text:
        if c in cyr_to_lat:
            out += cyr_to_lat[c]
        else:
            out += c
    return out


def lat2cyr(text):
    out = text
    # Covers case of "lj", "nj", "dž"
    two_letters_lat = [lat for lat in cyr_to_lat.values() if len(lat) == 2]
    for two_letter_lat in two_letters_lat:
        cyr_letter = next(key for key in cyr_to_lat.keys() if cyr_to_lat[key] == two_letter_lat)
        out = out.replace(two_letter_lat, cyr_letter)

    # Covers case of "LJ", "NJ", DŽ" (not in map directly)
    two_letters_lat_cap = [lat for lat in cyr_to_lat.values() if len(lat) == 2 and lat[0].isupper()]
    for two_letter_lat_cap in two_letters_lat_cap:
        cyr_letter = next(key for key in cyr_to_lat.keys() if cyr_to_lat[key] == two_letter_lat_cap)
        out = out.replace(two_letter_lat_cap.upper(), cyr_letter)
    one_letters_lat = [lat for lat in cyr_to_lat.values() if len(lat) == 1]
    for one_letter_lat in one_letters_lat:
        cyr_letter = next(key for key in cyr_to_lat.keys() if cyr_to_lat[key] == one_letter_lat)
        out = out.replace(one_letter_lat, cyr_letter)
    return out


def lat2int(text):
    out = ''
    for c in text:
        if c in lat_to_int:
            out += lat_to_int[c]
        else:
            out += c
    return out